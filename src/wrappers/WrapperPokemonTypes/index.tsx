import React, { useEffect, useState } from 'react'
import loadable from '@loadable/component'
import { WrapperPokemonTypeProps } from '../../interfaces/wrappers'
import { PokemonTypesResponse } from '../../interfaces/pokemon'
import { getPokemonBasedOnTypes } from '../../api'
import '../WrapperPokemonList/WrapperPokemonList.css'
import { PokemonTypes, TYPE_ALL } from '../../interfaces/pages'

const handleGetPokemonBasedOnTypes = async (
  type: PokemonTypes | number,
  updateState: React.Dispatch<React.SetStateAction<PokemonTypesResponse | undefined>>
): Promise<void> => {
  try {
    const response = await getPokemonBasedOnTypes(type)
    updateState(response.data)
  } catch (e) {
    console.log(e)
  }
}

const usePokemonListResponse = (type: PokemonTypes) => {
  const [pokemonListResponse, updatePokemonListResponse] = useState<PokemonTypesResponse>()
  useEffect(() => {
    if (!(TYPE_ALL.includes(type))) return
    handleGetPokemonBasedOnTypes(type, updatePokemonListResponse)
  }, [type])

  return pokemonListResponse
}

const ComponentPokemonList = loadable(() => import('../../components/ComponentPokemonList'))

const WrapperPokemonList = (props: WrapperPokemonTypeProps) => {
  const pokemonListResponse = usePokemonListResponse(props.type)
  const headerMessage = pokemonListResponse ?
    `Showing ${pokemonListResponse.pokemon.length} pokemons with ${pokemonListResponse.name} type` :
    `Invalid Type`
  const pokemons = pokemonListResponse?.pokemon.map(pokemon => <ComponentPokemonList key={pokemon.pokemon.name} pokemon={pokemon.pokemon} />)

  return (
    <article className="WrapperPokemonList container">
      <h1 className="WrapperPokemonList page-control">{ headerMessage }</h1>
      { pokemons }
    </article>
  )
}

export default WrapperPokemonList
