import { useParams } from 'react-router-dom'
import { PageDetailsParams } from '../../interfaces/pages'
import WrapperPokemonDetails from '../../wrappers/WrapperPokemonDetails'
import ComponentCompareTrigger from '../../components/ComponentCompareTrigger'
import './Details.css'
import { useState } from 'react'

const handleCompareTrigger = (pokeName: string, pokemonList: string[], updateState: React.Dispatch<React.SetStateAction<string[]>>) => {
  updateState([...pokemonList, pokeName])
}

const Details = () => {
  const { pokemon } = useParams<PageDetailsParams>()
  const [pokemonList, updatePokemonList] = useState<string[]>([pokemon])

  return (
    <article className="Details container">
      {pokemonList.map(pokeName => <WrapperPokemonDetails pokemon={pokeName} key={pokeName} />)}
      <ComponentCompareTrigger onCompareTrigger={poke => handleCompareTrigger(poke, pokemonList, updatePokemonList)}/>
    </article>
  )
}

export default Details
