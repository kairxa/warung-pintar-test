import ComponentDetail from '../../components/ComponentDetail'
import ComponentPredictionTrigger from '../../components/ComponentPredictionTrigger'
import { useEffect, useState } from 'react'
import { Pokemon, PokemonTypesResponse, TypeObject } from '../../interfaces/pokemon'
import { getPokemonBasedOnTypes, getPokemonDetail } from '../../api'
import './WrapperPrediction.css'

const handleCompareTrigger = (pokemonNames: string[], updateState: React.Dispatch<React.SetStateAction<string[]>>) => {
  updateState(pokemonNames)
}

const handleGetPokemonDetails = async (
  pokemon: string[],
  updateState: React.Dispatch<React.SetStateAction<Pokemon[]>>
): Promise<void> => {
  try {
    const response = await Promise.all(
      pokemon.map(async poke => {
        const detail = await getPokemonDetail(poke)
        return detail.data
      })
    )
    updateState(response)
  } catch (e) {
    console.log(e)
  }
}

const handleGetTypes = async (pokemonTypes: TypeObject[], updateState: React.Dispatch<React.SetStateAction<PokemonTypesResponse[]>>): Promise<void> => {
  try {
    const response = await Promise.all(
      pokemonTypes.map(async type => {
        const detail = await getPokemonBasedOnTypes(type.type.name)
        return detail.data
      })
    )
    updateState(response)
  } catch (e) {
    console.log(e)
  }
}

const usePokemonDetailsList = (pokemon: string[]) => {
  const [pokemonDetailsList, updatePokemonDetailsList] = useState<Pokemon[]>([])
  useEffect(() => {
    handleGetPokemonDetails(pokemon, updatePokemonDetailsList)
  }, [pokemon])

  return pokemonDetailsList
}

const useFirstPokemonTypesDetails = (pokemonTypes: TypeObject[]) => {
  const [pokemonTypesDetails, updatePokemonTypesDetails] = useState<PokemonTypesResponse[]>([])
  useEffect(() => {
    if (pokemonTypes) {
      handleGetTypes(pokemonTypes, updatePokemonTypesDetails)
    }
  }, [pokemonTypes])

  return pokemonTypesDetails
}

const WrapperPrediction = () => {
  // hackathon quality lel
  const [pokemonList, updatePokemonList] = useState<string[]>([])
  const pokemonDetailsList = usePokemonDetailsList(pokemonList)
  const firstPokemonTypes = pokemonDetailsList[0]?.types
  const firstPokemonTypesDetails = useFirstPokemonTypesDetails(firstPokemonTypes)
  const firstDamageRelations = firstPokemonTypesDetails.map(details => details.damage_relations)
  const doubleDamageFrom = firstDamageRelations.flatMap(detail => detail.double_damage_from.map(type => type.name))
  const doubleDamageTo = firstDamageRelations.flatMap(detail => detail.double_damage_to.map(type => type.name))
  const halfDamageFrom = firstDamageRelations.flatMap(detail => detail.half_damage_from.map(type => type.name))
  const halfDamageTo = firstDamageRelations.flatMap(detail => detail.half_damage_to.map(type => type.name))
  const noDamageFrom = firstDamageRelations.flatMap(detail => detail.no_damage_from.map(type => type.name))
  const noDamageTo = firstDamageRelations.flatMap(detail => detail.no_damage_to.map(type => type.name))
  const firstPokeTotalStats = pokemonDetailsList[0]?.stats.map(stat => stat.base_stat).reduce((prev, next) => prev + next)
  const secondPokeTotalStats = pokemonDetailsList[1]?.stats.map(stat => stat.base_stat).reduce((prev, next) => prev + next)
  const secondPokeTypes = pokemonDetailsList[1]?.types.map(type => type.type.name)
  
  let firstPokeStatsCoefficient = firstPokeTotalStats

  let additionalDisplay
  if (pokemonDetailsList.length < 2) {
    additionalDisplay = <ComponentPredictionTrigger onCompareTrigger={poke => handleCompareTrigger(poke, updatePokemonList)}/>
  } else {
    if (secondPokeTypes.some(type => doubleDamageFrom.includes(type))) {
      firstPokeStatsCoefficient *= 0.5
    }

    if (secondPokeTypes.some(type => doubleDamageTo.includes(type))) {
      firstPokeStatsCoefficient *= 2
    }

    if (secondPokeTypes.some(type => halfDamageFrom.includes(type))) {
      firstPokeStatsCoefficient *= 2
    }

    if (secondPokeTypes.some(type => halfDamageTo.includes(type))) {
      firstPokeStatsCoefficient *= 0.5
    }

    if (secondPokeTypes.some(type => noDamageFrom.includes(type))) {
      firstPokeStatsCoefficient *= 4
    }

    if (secondPokeTypes.some(type => noDamageTo.includes(type))) {
      firstPokeStatsCoefficient *= 0.25
    }

    const percentageWinning = ((firstPokeStatsCoefficient - secondPokeTotalStats) / secondPokeTotalStats * 100 + 50).toFixed(2)

    additionalDisplay = <section className="WrapperPrediction additional">
      <h3>
        Based on raw stats and pokemon types, {pokemonDetailsList[0]?.name} has {percentageWinning}% of winning against {pokemonDetailsList[1]?.name}
      </h3>
      <button onClick={() => handleCompareTrigger([], updatePokemonList)}>Reset</button>
    </section>
  }

  return (
    <article className="WrapperPrediction container">
      {pokemonDetailsList.map(pokemon => <ComponentDetail pokemon={pokemon} key={pokemon.id}/>)}
      {additionalDisplay}
    </article>
  )
}

export default WrapperPrediction
