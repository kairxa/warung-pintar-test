import { Link } from 'react-router-dom'
import { ComponentDetailProps } from '../../interfaces/components'
import './ComponentDetail.css'

const ComponentDetail = (props: ComponentDetailProps) => {
  const { pokemon } = props
  const totalStats = pokemon.stats.map(stat => stat.base_stat).reduce((prev, next) => prev + next)

  return (
    <section className="ComponentDetail container">
      <div>
        <img src={pokemon.sprites.other['official-artwork'].front_default} alt={pokemon.name} className="ComponentDetail image"/>
      </div>
      <span>Name: {pokemon.name}</span>
      <span>ID: {pokemon.id}</span>
      <h3>Types</h3>
      {pokemon.types.map(type => 
        <Link key={type.type.name} to={`/type/${type.type.name}`}>{type.type.name}</Link>  
      )}
      <h3>Stats</h3>
      {pokemon.stats.map(stat =>
        <span key={stat.stat.name}>{stat.stat.name}: {stat.base_stat} | EV: {stat.effort}</span>  
      )}
      <span>Total Stats: {totalStats}</span>
    </section>
  )
}

export default ComponentDetail
