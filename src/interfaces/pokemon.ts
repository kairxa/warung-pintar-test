import { PokemonTypes } from "./pages";

export interface Ability {
  name: string
  url: string
}

export interface AbilityObject {
  ability: Ability
  is_hidden: boolean
  slot: number
}

export interface Form {
  name: string
  url: string
}

export interface HeldItem {
  name: string;
  url: string;
}

export interface HeldItemObject {
  item: HeldItem
}

export interface Move {
  name: string;
  url: string;
}

export interface MoveObject {
  move: Move
}

export interface Species {
  name: string;
  url: string;
}

export interface SpriteOfficialArtwork {
  front_default: string
}

export interface SpriteOther {
  'official-artwork': SpriteOfficialArtwork
}

export interface Sprites {
  back_default: string
  back_female?: string
  back_shiny: string
  back_shiny_female?: string
  front_default: string
  front_female?: string
  front_shiny: string
  front_shiny_female?: string
  other: SpriteOther
}

export interface Stat {
  name: string
  url: string
}

export interface StatObject {
  base_stat: number
  effort: number
  stat: Stat
}

export interface Type {
  name: PokemonTypes
  url: string
}

export interface TypeObject {
  slot: number
  type: Type
}

export interface Pokemon {
  id: number
  abilities: AbilityObject[]
  base_experience: number
  forms: Form[]
  height: number
  held_items: HeldItemObject[]
  moves: MoveObject[]
  name: string
  order: number
  species: Species
  sprites: Sprites
  stats: StatObject[]
  types: TypeObject[]
}

export interface PokemonList {
  name: string
  url: string
}

export interface PokemonListResponse {
  count: number
  next?: string
  previous?: string
  results: PokemonList[]
}

export interface DamageType {
  name: string
  url: string
}

export interface DamageRelations {
  double_damage_from: DamageType[]
  double_damage_to: DamageType[]
  half_damage_from: DamageType[]
  half_damage_to: DamageType[]
  no_damage_from: DamageType[]
  no_damage_to: DamageType[]
}

export interface PokemonTypesResponse {
  name: string
  pokemon: {
    pokemon: PokemonList
  }[],
  damage_relations: DamageRelations
}
