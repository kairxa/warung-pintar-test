import { useState, useEffect } from 'react'
import { getPokemonDetail } from '../../api'
import { Pokemon } from '../../interfaces/pokemon'
import { WrapperPokemonDetailsProps } from '../../interfaces/wrappers'
import ComponentDetail from '../../components/ComponentDetail'
import './WrapperPokemonDetails.css'

const handleGetPokemonDetails = async (pokemon: number | string, updateState: React.Dispatch<React.SetStateAction<Pokemon | undefined>>): Promise<void> => {
  try {
    const response = await getPokemonDetail(pokemon)
    updateState(response.data)
  } catch (e) {
    console.log(e)
  }
}

const usePokemonDetailsResponse = (pokemon: number | string) => {
  const [pokemonDetailsResponse, updatePokemonDetailsResponse] = useState<Pokemon>()
  useEffect(() => {
    handleGetPokemonDetails(pokemon, updatePokemonDetailsResponse)
  }, [pokemon])

  return pokemonDetailsResponse
}

const WrapperPokemonDetails = (props: WrapperPokemonDetailsProps) => {
  const pokemonDetailsResponse = usePokemonDetailsResponse(props.pokemon)
  const displayed = pokemonDetailsResponse ? <ComponentDetail pokemon={pokemonDetailsResponse} /> : <h1>Invalid Pokemon name/ID</h1>

  return (
    <article className="WrapperPokemonDetails container">
      {displayed}
    </article>
  )
}

export default WrapperPokemonDetails
