export const TYPE_NORMAL = 'normal'
export const TYPE_FIGHTING = 'fighting'
export const TYPE_FLYING = 'flying'
export const TYPE_POISON = 'poison'
export const TYPE_GROUND = 'ground'
export const TYPE_ROCK = 'rock'
export const TYPE_BUG = 'bug'
export const TYPE_GHOST = 'ghost'
export const TYPE_STEEL = 'steel'
export const TYPE_FIRE = 'fire'
export const TYPE_WATER = 'water'
export const TYPE_GRASS = 'grass'
export const TYPE_ELECTRIC = 'electric'
export const TYPE_PSYCHIC = 'psychic'
export const TYPE_ICE = 'ice'
export const TYPE_DRAGON = 'dragon'
export const TYPE_DARK = 'dark'
export const TYPE_FAIRY = 'fairy'

export const TYPE_ALL = [
  TYPE_NORMAL, TYPE_FIGHTING, TYPE_FLYING, TYPE_POISON,
  TYPE_GROUND, TYPE_ROCK, TYPE_BUG, TYPE_GHOST,
  TYPE_STEEL, TYPE_FIRE, TYPE_WATER, TYPE_GRASS,
  TYPE_ELECTRIC, TYPE_PSYCHIC, TYPE_ICE, TYPE_DRAGON,
  TYPE_DARK, TYPE_FAIRY
] as const

export type PokemonTypes = typeof TYPE_ALL[number]

export interface PageBrowseParams {
  page?: string
  type?: PokemonTypes
}

export interface PageDetailsParams {
  pokemon: string
}
