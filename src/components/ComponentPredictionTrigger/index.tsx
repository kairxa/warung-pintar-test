import { useState } from 'react'
import { ComponentPredictionTriggerProps } from '../../interfaces/components'
import '../ComponentCompareTrigger/ComponentCompareTrigger.css'

const ComponentPredictionTrigger = (props: ComponentPredictionTriggerProps) => {
  const [firstPoke, updateFirstPoke] = useState('')
  const [secondPoke, updateSecondPoke] = useState('')

  return (
    <section className="ComponentCompareTrigger container">
      <h3>Predict Pokemon Win Rate</h3>
      <input type="text" placeholder="First Pokemon Name" value={firstPoke} onChange={(event) => updateFirstPoke(event.target.value)}/>
      <span>and</span>
      <input type="text" placeholder="Second Pokemon Name" value={secondPoke} onChange={(event) => updateSecondPoke(event.target.value)}/>
      <button onClick={
        () => {
          props.onCompareTrigger([firstPoke, secondPoke])
          updateFirstPoke('')
          updateSecondPoke('')
        }
      }>Predict</button>
    </section>
  )
}

export default ComponentPredictionTrigger
