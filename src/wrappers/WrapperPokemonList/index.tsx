import React, { useEffect, useState } from 'react'
import loadable from '@loadable/component'
import { Link } from 'react-router-dom'
import { WrapperPokemonListProps } from '../../interfaces/wrappers'
import { PokemonListResponse } from '../../interfaces/pokemon'
import { getPokemonList } from '../../api'
import './WrapperPokemonList.css'

const handleGetPokemonList = async (page: number, updateState: React.Dispatch<React.SetStateAction<PokemonListResponse | undefined>>): Promise<void> => {
  const limit = 100;
  const offset = page * limit;

  try {
    const response = await getPokemonList(limit, offset)
    updateState(response.data)
  } catch (e) {
    console.log(e)
  }
}

const usePokemonListResponse = (page: number) => {
  const [pokemonListResponse, updatePokemonListResponse] = useState<PokemonListResponse>()
  useEffect(() => {
    handleGetPokemonList(page, updatePokemonListResponse)
  }, [page])

  return pokemonListResponse
}

const ComponentPokemonList = loadable(() => import('../../components/ComponentPokemonList'))

const WrapperPokemonList = (props: WrapperPokemonListProps) => {
  const indexBasedPage = props.page - 1
  const pokemonListResponse = usePokemonListResponse(indexBasedPage)
  const pokemons = pokemonListResponse?.results.map(pokemon => <ComponentPokemonList key={pokemon.name} pokemon={pokemon} />)
  const firstPokeId = indexBasedPage * 100
  const lastPokeId = pokemonListResponse?.next ? props.page * 100 : pokemonListResponse?.count

  return (
    <article className="WrapperPokemonList container">
      <section className="WrapperPokemonList page-control">
        <h1>Showing Pokemon ID {firstPokeId}-{lastPokeId} of {pokemonListResponse?.count} </h1>
        {pokemonListResponse?.previous &&
          <Link to={`/browse/${props.page - 1}`} className="WrapperPokemonList link">Previous Page</Link>
        }
        {pokemonListResponse?.next &&
          <Link to={`/browse/${props.page + 1}`} className="WrapperPokemonList link">Next Page</Link>
        }
      </section>
      {pokemons}
    </article>
  )
}

export default WrapperPokemonList
