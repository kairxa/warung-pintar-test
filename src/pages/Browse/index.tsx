import loadable from '@loadable/component'
import { useParams } from 'react-router-dom'
import { PageBrowseParams } from '../../interfaces/pages'
import './Browse.css'

const WrapperPokemonList = loadable(() => import('../../wrappers/WrapperPokemonList'))
const WrapperPokemonTypes = loadable(() => import('../../wrappers/WrapperPokemonTypes'))

const Browse = () => {
  const { page, type } = useParams<PageBrowseParams>()

  return (
    <div>
      {page && <WrapperPokemonList page={Number(page)} />}
      {type && <WrapperPokemonTypes type={type}/>}
    </div>
  )
}

export default Browse
