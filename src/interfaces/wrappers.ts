import { PokemonTypes } from "./pages";

export interface WrapperPokemonListProps {
  page: number
}

export interface WrapperPokemonTypeProps {
  type: PokemonTypes
}

export interface WrapperPokemonDetailsProps {
  pokemon: string | number
}
