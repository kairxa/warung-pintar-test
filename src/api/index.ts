import axios, { AxiosResponse } from 'axios'
import { Pokemon, PokemonListResponse, PokemonTypesResponse } from '../interfaces/pokemon'

const BASE_URL = 'https://pokeapi.co/api/v2'
const POKEMON_LIST_URL = `${BASE_URL}/pokemon`
const POKEMON_TYPE_URL = `${BASE_URL}/type`

export const getPokemonList = (limit: number = 100, offset: number = 0): Promise<AxiosResponse<PokemonListResponse>> => axios.get(POKEMON_LIST_URL, {
  params: {
    limit,
    offset
  }
})

export const getPokemonDetail = (pokemon: number | string): Promise<AxiosResponse<Pokemon>> => axios.get(`${POKEMON_LIST_URL}/${pokemon}`)

export const getPokemonBasedOnTypes = (type: string | number): Promise<AxiosResponse<PokemonTypesResponse>> => axios.get(`${POKEMON_TYPE_URL}/${type}`)
