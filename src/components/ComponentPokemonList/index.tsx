import { Link } from 'react-router-dom'
import { ComponentPokemonListProps } from '../../interfaces/components'
import './ComponentPokemonList.css'

const ComponentPokemonList = (props: ComponentPokemonListProps) => {
  const pokemonId = props.pokemon.url.split('https://pokeapi.co/api/v2/pokemon/')[1].replace(/\//g, '')

  return (
    <Link to={`/details/${props.pokemon.name}`} className="ComponentPokemonList container">
      <img
        src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemonId}.png`}
        alt={props.pokemon.name}
        className="ComponentPokemonList img"
      />
      <span>{props.pokemon.name}</span>
    </Link>
  )
}

export default ComponentPokemonList
