import loadable from '@loadable/component';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import ComponentHeader from './components/ComponentHeader'

const BrowsePage = loadable(() => import('./pages/Browse'))
const DetailsPage = loadable(() => import('./pages/Details'))
const PredictionPage = loadable(() => import('./pages/Prediction'))

const App = () => {
  return (
    <Router>
      <ComponentHeader />
      <Switch>
        <Route path="/browse/:page">
          <BrowsePage />
        </Route>
        <Route path="/details/:pokemon">
          <DetailsPage />
        </Route>
        <Route path="/type/:type">
          <BrowsePage />
        </Route>
        <Route path="/prediction">
          <PredictionPage />
        </Route>
        <Route path="*">
          <Redirect to="/browse/1" />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
