import { useState } from 'react'
import { ComponentCompareTriggerProps } from '../../interfaces/components'
import './ComponentCompareTrigger.css'

const ComponentCompareTrigger = (props: ComponentCompareTriggerProps) => {
  const [pokeName, updatePokeName] = useState('')

  return (
    <section className="ComponentCompareTrigger container">
      <h3>Compare Pokemon</h3>
      <input type="text" placeholder="Pokemon Name" value={pokeName} onChange={(event) => updatePokeName(event.target.value)}/>
      <button onClick={
        () => {
          props.onCompareTrigger(pokeName)
          updatePokeName('')
        }
      }>Compare</button>
    </section>
  )
}

export default ComponentCompareTrigger
