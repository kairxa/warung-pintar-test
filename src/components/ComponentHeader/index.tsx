import { Link } from 'react-router-dom'
import { TYPE_ALL } from '../../interfaces/pages'
import './ComponentHeader.css'

const ComponentHeader = () => (
  <header className="ComponentHeader container">
    <nav className="ComponentHeader nav">
      <Link to="/" className="ComponentHeader nav-link">Home</Link>
      <Link to="/prediction" className="ComponentHeader nav-link">Win Rate Prediction</Link>
    </nav>
    <section className="ComponentHeader filter">
      <span>Filter by:</span>
      {TYPE_ALL.map(type => <Link to={`/type/${type}`} className="ComponentHeader filter-link">{type}</Link>)}
    </section>
  </header>
)

export default ComponentHeader
