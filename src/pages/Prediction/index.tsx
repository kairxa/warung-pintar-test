import WrapperPrediction from '../../wrappers/WrapperPrediction'

const Prediction = () => {
  return (
    <article>
      <WrapperPrediction />
    </article>
  )
}

export default Prediction
