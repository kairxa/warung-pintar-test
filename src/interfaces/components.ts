import { Pokemon, PokemonList } from './pokemon'

export interface ComponentPokemonListProps {
  pokemon: PokemonList
}

export interface ComponentDetailProps {
  pokemon: Pokemon
}

export interface ComponentCompareTriggerProps {
  onCompareTrigger(pokeName: string): void
}

export interface ComponentPredictionTriggerProps {
  onCompareTrigger(pokeNames: string[]): void
}
